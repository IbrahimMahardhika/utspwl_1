<!DOCTPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Halaman Produk</title>
  </head>
  <body> 
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <div class="container-fluid">
		<div class="collapse navbar-collapse" id="navbarNav">
		  <ul class="navbar-nav">
			<li class="nav-item">
			  <a class="nav-link active" aria-current="page" href="index.php">Home</a>
			</li>
			<li class="nav-item">
			  <a class="nav-link" href="#">Produk</a>
			</li>
			<li>
			<a href="index.php">Logout</a>
			</li>
			
		  </ul>
		</div>
	  </div>
	</nav>    

	<div class="container">
	<?php
	  require_once'database.php';
	  $db=new Database();
	  $hasil=$db->produkAll();  
	  echo "<a href='addBrg.php' class='btn btn-success text-white'>Tambah Data</a>";

	  if ($hasil->num_rows>0) {
		echo "<table class='table table-striped'>
		   <thead>
			<tr>					
			<th scope='col'>ID</th>
			<th scope='col'>Nama</th>
			<th scope='col'>Harga</th>
			<th scope='col'>Stok</th>
			<th scope='col'>Keterangan</th>
			<th scope='col'>Foto</th>
			<th scope='col'>Edit</th>
			<th scope='col'>Hapus</th>
			</tr>
		    </thead>
		    <tbody>";
			while ($row=$hasil->fetch_assoc()){
				$teks="<tr>";
				$teks.="<td>".$row["id"]."</td>";
				$teks.="<td>".$row["nama"]."</td>";			
				$teks.="<td>".$row["hrg"]."</td>";
				$teks.="<td>".$row["jml"]."</td>";
				$teks.="<td>".$row["keterangan"]."</td>";
				$teks.="<td><img src='img/".$row["foto"]."' style='width:100px;height:100px;'></img></td>";
				$teks.="<td><a href='editBrg.php?id=".htmlentities($row["id"])."'>Edit</a></td>";
				$teks.="<td><a href='delBrg.php?id=".htmlentities($row["id"])."'>Hapus</a></td>";
				$teks.="</tr>";	
				echo $teks;
			}		
			echo "</tbody>
			      </table>";
		
		} else {
			echo "jml rec: 0 " ;
		}
		//$conn->close();
	?>	

	</div>	
  </body>
</html>
 
